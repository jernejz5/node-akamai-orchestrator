const EdgeGrid = require('edgegrid');
const _ = require('underscore');
const VError = require('verror');
const logger = require('log4js').getLogger('open-api');

/**
 * Executes Open API command.
 * @param {Object} programn - Commander program object which holds general parameters.
 * @param {Object} opt - Command options object
 * @param {String} opt.credentials - Credentials section in .edgerc file to be used.
 * @param {String} opt.path - Request path.
 * @param {String} opt.method - Request method.
 * @param {String} opt.headers - Request headers.
 * @param {String} opt.body - Request body.
 * @param {Object} opt.queries - Query string object.
 * @param {Function} cb - Callback function. First argument is error, second argument is data.
 */

module.exports = function api(program, param, cb) {
  const credentialsDefault = program.credentials || process.env.OPENAPI_DEFAULT.toUpperCase().replace('-', '_');

  const credentials = `${credentialsDefault}${program.context ? '-global' : ''}`;

  const credentialsEnv = credentials.toUpperCase().replace('-', '_');
  const opt = {
    clientToken: process.env[`OPENAPI_${credentialsEnv}_CLIENT_TOKEN`],
    clientSecret: process.env[`OPENAPI_${credentialsEnv}_CLIENT_SECRET`],
    accessToken: process.env[`OPENAPI_${credentialsEnv}_ACCESS_TOKEN`],
    host: process.env[`OPENAPI_${credentialsEnv}_HOST`],
    credentials,
    path: param.path || '/',
    qs: program.context
      ? _.extend({
        accountSwitchKey: program.context,
      }, param.queries, {})
      : _.extend(param.queries, {}),
    headers: param.headers || {},
    method: param.method || 'GET',
    body: param.body,
  };

  logger.trace(`API call with options: ${JSON.stringify(opt, null, 2)}`);

  const eg = new EdgeGrid(
    opt.clientToken,
    opt.clientSecret,
    opt.accessToken,
    opt.host,
  );

  eg.auth(opt)
    .send((err, response, body) => {
      if (err) {
        return cb(new VError(err, 'Error during Open API call.'));
      }

      let parsedBody = null;
      try {
        parsedBody = JSON.parse(body);
      } catch (e) {
        logger.warn('Unable to parse response body to JSON');
      }

      const okStatusCodes = [200, 201, 204];
      if (!okStatusCodes.includes(response.statusCode)) {
        return cb(new VError({
          name: '',
          info: {
            status: response.statusCode,
            message: response.statusMessage,
            response: parsedBody || body,
          },
        },
        'Open API gateway returned unexpected response.\n%s %s\n%s',
        response.statusCode,
        response.statusMessage,
        parsedBody ? JSON.stringify(parsedBody, null, 2) : body));
      }

      logger.trace(`Open API response: \n${parsedBody ? JSON.stringify(parsedBody, null, 2) : body}`);

      return cb(null, parsedBody || body);
    });
};
