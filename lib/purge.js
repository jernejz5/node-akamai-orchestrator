const util = require('util');
const _ = require('underscore');
const VError = require('verror');
const logger = require('log4js').getLogger('purge');

const openApi = require('./open-api');

const headers = { 'Content-Type': 'application/json' };

module.exports.use = function use(program, programCb) {
  logger.debug('Setting up purge command');
  program
    .command('purge')
    .description('Purge or invalidate content on Akamai Edge servers')
    .option('--invalidate', 'Execute invalidation instead of purge')
    .option('--production', 'Execute on production network')
    .option('--url [url]', 'URLs to purge', util.collectOptions, [])
    .option('--cpcode [cpcode]', 'CPCODEs to purge', util.collectOptions, [])
    .option('--tag [tag]', 'TAGs to purge', util.collectOptions, [])
    .action((opt) => {
      logger.trace('Running cache purge.');
      const action = opt.invalidate ? 'invalidate' : 'delete';
      const network = opt.production ? 'production' : 'staging';

      let objectList = [];
      let type = 'url';
      if (!_.isEmpty(opt.url)) {
        type = 'url';
        objectList = opt.url;
      } else if (!_.isEmpty(opt.cpcode)) {
        type = 'cpcode';
        objectList = opt.cpcode;
      } else if (!_.isEmpty(opt.tag)) {
        type = 'tag';
        objectList = opt.tag;
      } else {
        return programCb(new VError('Missing parameter. Either url, cpcode or tag needs to be passed.'));
      }

      openApi(
        program,
        {
          path: `/ccu/v3/${action}/${type}/${network}`,
          method: 'POST',
          credentials: 'purge',
          headers,
          body: {
            objects: [
              objectList,
            ],
          },
        },
        (err, data) => programCb(err ? new VError(err, 'Error running cache purge [Action: %s, Type:, Network: %s]', action, type, network) : null, data, program),
      );

      return null;
    });
};
