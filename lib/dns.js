const fs = require('fs');
const async = require('async');
const _ = require('underscore');
const VError = require('verror');
const uuid = require('uuid/v1');
const logger = require('log4js').getLogger('dns');

const openApi = require('./open-api');

const headers = { 'Content-Type': 'application/json' };

function getZone(program, zoneName, cb) {
  return openApi(
    program,
    {
      path: `/config-dns/v1/zones/${zoneName}`,
      method: 'GET',
    },
    (err, data) => cb(err ? new VError(err, 'Error retrieving zone %s', zoneName) : null, data),
  );
}

function updateZone(program, zoneName, opt, cb) {
  return async.waterfall(
    [
      (cbWaterfall) => {
        logger.trace('Reading zone file.');
        fs.readFile(opt.zoneFile, (err, data) => {
          if (err) return cbWaterfall(new VError(err, 'Error reading zone file %s', opt.zoneFile));
          const zoneData = JSON.parse(data.toString());
          return cbWaterfall(null, { zone: zoneData.zone, token: zoneData.token });
        });
      },
      (zone, cbWaterfall) => {
        logger.trace('Updating DNS zone.');
        return openApi(
          program,
          {
            path: `/config-dns/v1/zones/${zoneName}`,
            method: 'POST',
            headers,
            body: zone,
          },
          (err, data) => cbWaterfall(err ? new VError(err, 'Error updating zone %s.', zoneName) : null, data),
        );
      },
    ],
    (err, data) => cb(err, data),
  );
}

module.exports.use = function use(program, programCb) {
  program
    .command('dns-get-zone <zoneName>')
    .description('Retrieve a zone configuration')
    .action((zoneName) => {
      logger.trace(`Retrieving DNS zone ${zoneName}.`);
      if (!zoneName) return programCb(new VError('Missing zoneName parameter.'));
      return getZone(program, zoneName, (err, data) => programCb(err, data, program));
    });

  program
    .command('dns-update-zone <zoneName>')
    .description('Update a zone configuration')
    .option('--zoneFile [zoneFile]', 'Zone file')
    .action((zoneName, opt) => {
      logger.trace(`Updating DNS zone ${zoneName}. Options: ${opt}`);
      if (!zoneName) return programCb(new VError('Missing zoneName parameter.'));
      if (!opt.zoneFile) return programCb(new VError('Missing zoneFile parameter.'));
      return updateZone(program, zoneName, opt, (err, data) => programCb(err, data, program));
    });

  program
    .command('dns-change-zone <zoneName>')
    .description('Change a zone configuration')
    .action((zoneName, opt) => {
      logger.trace(`Changing DNS zone ${zoneName}. Options: ${opt}`);
      if (!zoneName) return programCb(new VError('Missing zoneName parameter.'));
      return async.waterfall([
        cb => getZone(program, zoneName, cb),
        (zone, cb) => {
          const newZone = _.extend(zone, { tempFile: `/tmp/${uuid()}.json` });
          return fs.writeFile(newZone.tempFile, JSON.stringify(newZone, null, '\t'), (errWrite) => {
            if (errWrite) return cb(new VError(errWrite, 'Error writing output to file'));
            logger.debug(`Temporary zone written to file: ${newZone.tempFile}`);
            return cb(null, newZone);
          });
        },
        (zone, cb) => {
          logger.info(`You can now make changes to temporary file ${zone.tempFile} . Press any key once ready to proceed.`);
          process.stdin.once('data', () => {
            cb(null, zone);
          });
        },
        (zone, cb) => updateZone(program, zoneName, { zoneFile: zone.tempFile }, cb),
      ],
      (err, data) => programCb(err ? new VError(err, 'Error changing zone %s.', zoneName) : null, data, program));
    });
};
