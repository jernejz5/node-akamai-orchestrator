module.exports.collectOptions = function collectOptions(val, list) {
  list.push(val);
  return list;
};

module.exports.parseParameterList = function parseParameterList(parameterList, delimiter) {
  const parameterObj = {};
  parameterList.forEach((parameter) => {
    if (parameter.indexOf(delimiter) === -1) { parameterObj[parameter] = ''; } else {
      const name = parameter.substring(0, parameter.indexOf(delimiter)).trim();
      const value = parameter.substring(parameter.indexOf(delimiter) + 1).trim();
      parameterObj[name] = value;
    }
  });
  return parameterObj;
};
