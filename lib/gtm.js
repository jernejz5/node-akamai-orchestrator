const logger = require('log4js').getLogger('gtm');
const VError = require('verror');

const openApi = require('./open-api');

module.exports.use = function use(program, programCb) {
  logger.debug('Setting up gtm command');
  program
    .command('gtm-get-domain <domain>')
    .description('Get GTM domain information')
    .action((domain) => {
      logger.trace('Retrieving GTM domain.');
      return openApi(
        program,
        {
          path: `/config-gtm/v1/domains/${domain}`,
          method: 'GET',
        },
        (err, data) => programCb(err ? new VError(err, 'Error retrieving GTM domain %s', domain) : null, data, program),
      );
    });
};
