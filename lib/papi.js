const async = require('async');
const fs = require('fs');
const _ = require('underscore');
const uuid = require('uuid/v1');
const VError = require('verror');
const logger = require('log4js').getLogger('papi');

const openApi = require('./open-api');
const util = require('./util');

const headers = {
  'Content-Type': 'application/json',
  'PAPI-Use-Prefixes': 'true',
};

function getNotificationEmail(opt) {
  logger.trace(`Parsing notification Email value. opt.notificationEmail=${opt.notificationEmail}, process.env.PAPI_NOTIFICATION_EMAIL=${process.env.PAPI_NOTIFICATION_EMAIL}`);
  if (!_.isEmpty(opt.notificationEmail)) {
    return opt.notificationEmail;
  }
  if (process.env.PAPI_NOTIFICATION_EMAIL) {
    return process.env.PAPI_NOTIFICATION_EMAIL.split(',');
  }
  return [];
}

function searchPropertyByName(program, property, cb) {
  logger.debug(`Searching for property name ${property.propertyName}.`);
  return openApi(
    program,
    {
      path: '/papi/v1/search/find-by-value',
      method: 'POST',
      headers,
      body: {
        propertyName: property.propertyName,
      },
    },
    (err, data) => {
      if (err || !data || !data.versions || !data.versions.items) {
        return cb(new VError(err, 'Property results are empty.'));
      }
      const apiProperty = _.find(data.versions.items, (i) => {
        logger.trace(`Processig search result ${JSON.stringify(i)}`);
        return i.propertyName === property.propertyName;
      });
      if (!apiProperty || !apiProperty.propertyId) {
        return cb(new VError('Property not found.'));
      }
      return cb(null, _.extend(apiProperty, property));
    },
  );
}

function retrieveProperty(program, property, cb) {
  logger.debug(`Retrieving property ${property.propertyId}.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.properties || _.isEmpty(data.properties.items)) {
        return cb(new VError(err, 'Property does not exist.'));
      }
      return cb(null, _.extend(data.properties.items[0], property));
    },
  );
}

function retrieveAllPropertyVersions(program, property, cb) {
  logger.debug(`Retrieving all property ${property.propertyId} versions.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.versions || _.isEmpty(data.versions.items)) {
        return cb(new VError(err, 'Property versions do not exist.'));
      }
      return cb(null, _.extend(property, { versions: data.versions.items }));
    },
  );
}

function retrieveAllPropertyActivations(program, property, cb) {
  logger.debug(`Retrieving all property ${property.propertyId} activations.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/activations`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.activations || _.isEmpty(data.activations.items)) {
        return cb(new VError(err, 'Property activations do not exist.'));
      }
      return cb(null, _.extend(property, { activations: data.activations.items }));
    },
  );
}

function retrieveLatestPropertyVersion(program, property, cb) {
  logger.debug(`Retrieving property ${property.propertyId} version ${property.latestVersion}.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions/${property.latestVersion}`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.versions || _.isEmpty(data.versions.items)) {
        return cb(new VError('Property %s version %s does not exist.', property.propertyId, property.latestVersion));
      }
      return cb(null, _.extend(property, data.versions.items[0]));
    },
  );
}

function retrievePropertyRules(program, property, cb) {
  logger.debug(`Retrieving property ${property.propertyId} rules.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions/${property.propertyVersion}/rules`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.rules) {
        return cb(new VError(err, 'Property rules not retrieved.'));
      }
      return cb(null, _.extend(property, {
        rules: data.rules,
        warnings: data.warnings,
        ruleFormat: data.ruleFormat,
      }));
    },
  );
}

function retrievePropertyHostnames(program, property, cb) {
  logger.debug(`Retrieving property ${property.propertyId} hostnames.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions/${property.propertyVersion}/hostnames`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.hostnames || !data.hostnames.items) {
        return cb(new VError(err, 'Property hostnames not retrieved.'));
      }
      return cb(err ? new VError(err, 'Error retrieving property hostnames') : null, _.extend(property, { hostnames: data.hostnames.items }));
    },
  );
}

function retrievePropertyBehaviors(program, property, cb) {
  logger.debug(`Retrieving property ${property.propertyId} behaviors.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions/${property.propertyVersion}/available-behaviors`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.behaviors || !data.behaviors.items) {
        return cb(new VError(err, 'Property behaviors not retrieved.'));
      }
      return cb(err ? new VError(err, 'Error retrieving property behaviors') : null, _.extend(property, { behaviors: data.behaviors.items }));
    },
  );
}

function retrievePropertyCriteria(program, property, cb) {
  logger.debug(`Retrieving property ${property.propertyId} criteria.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions/${property.propertyVersion}/available-criteria`,
      headers,
    },
    (err, data) => {
      if (err || !data || !data.criteria || !data.criteria.items) {
        return cb(new VError(err, 'Property criteria not retrieved.'));
      }
      return cb(err ? new VError(err, 'Error retrieving property criteria') : null, _.extend(property, { criteria: data.criteria.items }));
    },
  );
}

function retrievePropertyRuleSchema(program, property, cb) {
  logger.debug(`Retrieving property ${property.propertyId} rule schema.`);
  return openApi(
    program,
    {
      path: `/papi/v1/schemas/products/${property.productId}/${property.ruleFormat}`,
      headers,
    },
    (err, data) => {
      if (err || !data) {
        return cb(new VError(err, 'Property rule schema not retrieved.'));
      }
      return cb(err ? new VError(err, 'Error retrieving property schema') : null, _.extend(property, { ruleSchema: data }));
    },
  );
}

function createNewPropertyVersion(program, property, cb) {
  logger.debug(`Create new version if neccesary. Property ${property.propertyId} based on version ${property.latestVersion}.`);
  if (!property.stagingStatus === 'INACTIVE' || !property.stagingStatus === 'INACTIVE') return cb(new VError('Property version is already activated. Either create new version or use another version.'));
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions`,
      method: 'POST',
      headers,
      body: {
        createFromVersion: property.propertyVersion,
        createFromVersionEtag: property.etag,
      },
    },
    (err, data) => {
      if (!data || !data.versionLink) {
        return cb(new VError(err, 'New property version does not exist.'));
      }
      return openApi(
        program,
        {
          path: data.versionLink,
          headers,
        },
        (errNew, dataNew) => {
          if (
            errNew
            || !dataNew
            || !dataNew.versions
            || _.isEmpty(dataNew.versions.items)) {
            return cb(new VError(errNew, 'Property version does not exist.'));
          }
          return cb(err ? new VError(err, 'Error retrieving property versions') : null, _.extend(property, dataNew.versions.items[0]));
        },
      );
    },
  );
}

function updateRules(program, property, cb) {
  logger.debug(`Update rules for property ${property.propertyId} version ${property.propertyVersion}.`);
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/versions/${property.propertyVersion}/rules`,
      method: 'PUT',
      headers,
      queries: {
        validateMode: 'full',
        validateRules: 'true',
      },
      body: {
        rules: property.rules,
      },
    },
    (err, data) => {
      if (err || !data || !data.etag) {
        return cb(new VError(err, 'Property rules were not updated.'));
      }
      return cb(err ? new VError(err, 'Error updating property rules') : null, _.extend(property, { etag: data.etag, errors: data.errors }));
    },
  );
}

function listAllContracts(program, cb) {
  logger.info('Listing all contracts');
  return openApi(
    program,
    {
      path: '/papi/v1/contracts',
      headers,
    },
    (err, data) => cb(err ? new VError(err, 'Error listing contracts.') : null, data),
  );
}

function listAllGroups(program, cb) {
  logger.info('Listing all groups');
  return openApi(
    program,
    {
      path: '/papi/v1/groups',
      headers,
    },
    (err, data) => cb(err ? new VError(err, 'Error listing groups.') : null, data, program),
  );
}

function activatePropertyLatestVersion(program, property, opt, cb) {
  logger.debug(`Add new activation/deactivation for property ${property.propertyId} version ${property.latestVersion}.`);
  if (_.isEmpty(getNotificationEmail(opt))) return cb(new VError('At least one notification email must be specified.'));
  const compliance = opt.complianceNoProdTraffic
    ? { noncomplianceReason: 'NO_PRODUCTION_TRAFFIC' } : null;
  return openApi(
    program,
    {
      path: `/papi/v1/properties/${property.propertyId}/activations`,
      method: 'POST',
      headers,
      body: {
        propertyVersion: property.propertyVersion,
        network: opt.production ? 'PRODUCTION' : 'STAGING',
        note: opt.note ? opt.note : 'API activation.',
        notifyEmails: getNotificationEmail(opt),
        acknowledgeWarnings: [],
        useFastFallback: opt.rollback,
        complianceRecord: opt.complianceNoProdTraffic ? compliance : undefined,
      },
    },
    (err, data) => {
      if (err && VError.info(err) && VError.info(err).response && VError.info(err).response.warnings && VError.info(err).response.type === 'https://problems.luna.akamaiapis.net/papi/v0/activation/warnings-not-acknowledged') {
        const messageIds = [];
        _.each(VError.info(err).response.warnings, (warning) => {
          messageIds.push(warning.messageId);
        });
        return openApi(
          program,
          {
            path: `/papi/v1/properties/${property.propertyId}/activations`,
            method: 'POST',
            headers,
            body: {
              propertyVersion: property.propertyVersion,
              network: opt.production ? 'PRODUCTION' : 'STAGING',
              note: opt.note ? opt.note : 'API activation.',
              notifyEmails: getNotificationEmail(opt),
              acknowledgeWarnings: messageIds,
              useFastFallback: opt.rollback,
              complianceRecord: opt.complianceNoProdTraffic ? compliance : undefined,
            },
          },
          (errAck, dataAck) => {
            if (errAck || !dataAck || !dataAck.activationLink) {
              return cb(new VError({
                name: 'AcvtivationError',
                cause: errAck,
                info: errAck.response,
              },
              'Property activation did not suceed.'));
            }
            return cb(null, _.extend(
              property,
              { activationLink: dataAck.activationLink },
            ));
          },
        );
      }
      if (err || !data || !data.activationLink) {
        return cb(new VError(err, 'Property activation did not suceed.'));
      }
      return cb(null, _.extend(property, { activationLink: data.activationLink }));
    },
  );
}

function followPropertyActivation(program, property, cb) {
  logger.debug(`Following activation for property ${property.propertyId} version ${property.latestVersion}.`);
  if (!property.activationLink) return cb(new VError('Activation link is not present.'));
  return async.retry(
    { times: 540, interval: 20000 },
    (cbActivationStatus) => {
      logger.debug(`Following up with activation ${property.activationLink}`);
      return openApi(
        program,
        {
          path: `${property.activationLink}`,
          headers,
        },
        (err, data) => {
          if (err || !data || !data.activations || _.isEmpty(data.activations.items)) {
            return cbActivationStatus(new VError(err, 'Property activation status retrieval failed.'));
          }
          if (data.activations.items[0].status === 'ACTIVE') {
            logger.info('Property activation complete.');
            return cbActivationStatus(null, data.activations.items[0]);
          }
          if (data.activations.items[0].status === 'PENDING') {
            logger.info('Property activation in progress.');
            return cbActivationStatus(new VError('Property activation in progress.'));
          }
          return cbActivationStatus(new VError('Unable to follow activation link %s. Activation %s', property.activationLink, data.activations.items[0].status));
        },
      );
    },
    (err, data) => cb(err ? new VError(err, 'Error following property activation.') : null, data),
  );
}

module.exports.use = function use(program, programCb) {
  program
    .command('papi-list-contracts')
    .description('List all contracts')
    .action(() => {
      listAllContracts(program, (err, data) => programCb(err, data, program));
    });

  program
    .command('papi-list-groups')
    .description('List all groups')
    .action(() => {
      listAllGroups(program, (err, data) => programCb(err, data, program));
    });

  program
    .command('papi-list-properties')
    .description('List all properties with filters')
    .option('--groupName [groupName]', 'List only properties in specific group')
    .action((opt) => {
      logger.info('Listing all properties.');
      return async.waterfall(
        [
          (cb) => {
            logger.debug('Retrieving groups.');
            return listAllGroups(program, (err, data) => {
              if (err || !data || !data.groups || !data.groups.items) return cb(new VError(err, 'Groups not found'));
              return cb(`err`, data.groups.items);
            });
          },
          (groups, cb) => {
            async.reduce(
              groups,
              [],
              (memo, item, cbReduce) => {
                logger.debug('Reducing group.');
                if (opt.groupName && opt.groupName !== item.groupName) return cbReduce(null, memo);
                return openApi(
                  program,
                  {
                    path: '/papi/v1/properties',
                    queries: {
                      groupId: item.groupId,
                      contractId: item.contractIds[0],
                    },
                    headers,
                  },
                  (err, data) => {
                    if (err) logger.debug(`Error retrieving properties for group: ${err}`);
                    if (!data || !data.properties || !data.properties.items) {
                      return cbReduce(null, memo);
                    }
                    return cbReduce(null, memo.concat(data.properties.items));
                  },
                );
              },
              cb,
            );
          },
        ],
        (err, data) => programCb(err, data, program),
      );
    });

  program
    .command('papi-get-property <propertyName>')
    .description('Retrieve property')
    .option('--versions', 'Retrieve all property versions')
    .option('--activations', 'Retrieve all property activations')
    .option('--rules', 'Retrieve property version rules')
    .option('--hostnames', 'Retrieve property version hostnames')
    .option('--behaviors', 'Retrieve available behaviors')
    .option('--criteria', 'Retrieve available criteria')
    .option('--ruleSchema', 'Retrieve available criteria')
    .action((propertyName, opt) => {
      logger.info(`Retrieving property version details for ${propertyName}.`);
      if (!propertyName) return programCb(new VError('PropertyName is not defined.'));
      return async.waterfall(
        [
          cb => searchPropertyByName(program, { propertyName }, cb),
          (property, cb) => retrieveProperty(program, property, cb),
          (property, cb) => {
            if (!opt.versions) return cb(null, property);
            return retrieveAllPropertyVersions(program, property, cb);
          },
          (property, cb) => {
            if (!opt.activations) return cb(null, property);
            return retrieveAllPropertyActivations(program, property, cb);
          },
          (property, cb) => retrieveLatestPropertyVersion(program, property, cb),
          (property, cb) => {
            if (!opt.rules) return cb(null, property);
            return retrievePropertyRules(program, property, cb);
          },
          (property, cb) => {
            if (!opt.hostnames) return cb(null, property);
            return retrievePropertyHostnames(program, property, cb);
          },
          (property, cb) => {
            if (!opt.behaviors) return cb(null, property);
            return retrievePropertyBehaviors(program, property, cb);
          },
          (property, cb) => {
            if (!opt.criteria) return cb(null, property);
            return retrievePropertyCriteria(program, property, cb);
          },
          (property, cb) => {
            if (!opt.ruleSchema) return cb(null, property);
            return retrievePropertyRuleSchema(program, property, cb);
          },
        ],
        (err, data) => programCb(err, data, program),
      );
    });

  program
    .command('papi-compare-versions <propertyName>')
    .description('Retrieve property')
    .option('--versions [versions]', 'Versions to compare delimited by comma')
    .option('--versionFile [versionFile]', 'Write each version to a seperate file appended by version number and .json')
    .action((propertyName, opt) => {
      logger.info(`Comparing property ${propertyName} versions ${opt.versions}.`);
      if (!propertyName) return programCb(new VError('PropertyName is not defined.'));
      return async.waterfall(
        [
          cb => searchPropertyByName(program, { propertyName }, cb),
          (property, cb) => retrieveProperty(program, property, cb),
          (property, cb) => async.map(
            _.uniq(opt.versions
              ? opt.versions.split(',')
              : [property.latestVersion, property.productionVersion, property.stagingVersion]),
            (version, cbVersion) => async.waterfall(
              [
                cbRules => retrievePropertyRules(
                  program,
                  { propertyId: property.propertyId, propertyVersion: version },
                  cbRules,
                ),
                (versionRules, cbHostnames) => retrievePropertyHostnames(
                  program,
                  {
                    propertyId: property.propertyId,
                    propertyVersion: version,
                    rules: versionRules.rules,
                  },
                  cbHostnames,
                ),
                (versionData, cbVersionData) => {
                  if (!opt.versionFile) return cbVersionData(null, versionData);
                  return fs.writeFile(
                    `${opt.versionFile}_${versionData.propertyVersion}.json`,
                    JSON.stringify(versionData, null, 2),
                    (errWrite) => {
                      if (errWrite) return cbVersionData('Error writing version file.');
                      return cbVersionData(null, versionData);
                    },
                  );
                },
              ],
              cbVersion,
            ),
            cb,
          ),
        ],
        (err, data) => programCb(err, data, program),
      );
    });

  program
    .command('papi-update-property <propertyName>')
    .description('Update property version')
    .option('--newVersion', 'Create new property version')
    .option('--rulesFile [rulesFile]', 'Update property version rules')
    .action((propertyName, opt) => {
      logger.info(`Update property ${propertyName} version.`);
      if (!propertyName) return programCb(new VError('PropertyName is not defined.'));
      return async.waterfall(
        [
          cb => searchPropertyByName(program, { propertyName }, cb),
          (property, cb) => retrieveProperty(program, property, cb),
          (property, cb) => retrieveLatestPropertyVersion(program, property, cb),
          (property, cb) => createNewPropertyVersion(program, property, cb),
          (property, cb) => {
            logger.debug(`Reading new rules from ${opt.rulesFile}.`);
            if (!opt.rulesFile) return cb(null, property);
            return fs.readFile(opt.rulesFile, (err, data) => {
              if (err) return cb(new VError(err, 'Unable to read rules file %s', opt.rulesFile));
              let jsonData = data;
              try {
                jsonData = JSON.parse(data);
              } catch (e) {
                return cb(new VError('Unable to parse rules file %s', opt.rulesFile));
              }
              if (err || !jsonData || !jsonData.rules) return cb(new VError(err, 'Error reading rules file %s', opt.rulesFile));
              return cb(null, _.extend(property, { rules: jsonData.rules }));
            });
          },
          (property, cb) => updateRules(program, property, cb),
        ],
        (err, data) => programCb(err, data, program),
      );
    });

  program
    .command('papi-activate <propertyName>')
    .description('Activate a property on Akamai Network')
    .option('--version [version]', 'Specify version to activat: last version')
    .option('--note [note]', 'Note to be aded to the activation')
    .option('--notificationEmail [notifiacationEmailAddress]', 'Email adresses to be notified upon job completed: none', util.collectOptions, [])
    .option('--rollback', 'Do rollback')
    .option('--production', 'Do activation on production network')
    .option('--complianceNoProdTraffic', 'Production activation with no production traffic compliancy')
    .option('--complianceNoProdTraffic', 'Production activation with no production traffic compliancy')
    .action((propertyName, opt) => {
      logger.info(`Activating property ${propertyName}.`);
      if (!propertyName) return programCb(new VError('PropertyName is not defined.'));
      return async.waterfall(
        [
          cb => searchPropertyByName(program, { propertyName }, cb),
          (property, cb) => retrieveProperty(program, property, cb),
          (property, cb) => retrieveLatestPropertyVersion(program, property, cb),
          (property, cb) => activatePropertyLatestVersion(program, property, opt, cb),
          (property, cb) => followPropertyActivation(program, property, cb),
        ],
        (err, data) => programCb(err, data, program),
      );
    });

  program
    .command('papi-change <propertyName>')
    .description('Change property name and deploy it to Akamai Network')
    .option('--note [note]', 'Version note')
    .option('--newVersion', 'Create new property version')
    .option('--notificationEmail [notificationEmailAddress]', 'Email adresses to be notified upon job completed: none', util.collectOptions, [])
    .option('--complianceNoProdTraffic', 'Compliance record needs to be specified')
    .action((propertyName, opt) => {
      logger.info(`Retrieving property ${propertyName} information.`);
      if (!propertyName) return programCb(new VError('PropertyName is not defined.'));
      if ((opt.staging && _.isEmpty(getNotificationEmail(opt)))
        || (opt.complianceNoProdTraffic && _.isEmpty(getNotificationEmail(opt)))) return programCb(new VError('At least one notification email must be specified.'));

      return async.waterfall(
        [
          cb => searchPropertyByName(program, { propertyName }, cb),
          (property, cb) => retrieveProperty(program, property, cb),
          (property, cb) => retrieveLatestPropertyVersion(program, property, cb),
          (property, cb) => async.parallel([
            cbParallel => retrieveAllPropertyVersions(program, property, cbParallel),
            cbParallel => retrieveAllPropertyActivations(program, property, cbParallel),
            cbParallel => retrievePropertyRules(program, property, cbParallel),
            cbParallel => retrievePropertyHostnames(program, property, cbParallel),
            cbParallel => retrievePropertyBehaviors(program, property, cbParallel),
            cbParallel => retrievePropertyCriteria(program, property, cbParallel),
            cbParallel => retrievePropertyRuleSchema(program, property, cbParallel),
          ], (err, results) => {
            if (err) {
              return cb(err);
            }
            return cb(
              null,
              _.extend(
                property,
                { versions: results[0].versions },
                { activations: results[1].activations },
                { rules: results[2].rules },
                { hostnames: results[3].hostnames },
                { behaviors: results[4].behaviors },
                { criteria: results[5].criteria },
                { schema: results[6].schema },
              ),
            );
          }),
          (property, cb) => {
            const newProperty = _.extend(property, { tempFile: `/tmp/${uuid()}.json` });
            return fs.writeFile(newProperty.tempFile, JSON.stringify(newProperty, null, '\t'), (errWrite) => {
              if (errWrite) return cb(new VError('Error writing output to file'));
              logger.debug(`Temporary property written to file: ${newProperty.tempFile}`);
              return cb(null, newProperty);
            });
          },
          (property, cb) => {
            logger.info(`You can now make changes to temporary file ${property.tempFile} . Press any key once ready to proceed.`);
            process.stdin.once('data', () => {
              cb(null, property);
            });
          },
          (property, cb) => {
            logger.info(`Reading new rules from ${property.tempFile}.`);
            return fs.readFile(property.tempFile, (err, data) => {
              let jsonData = data;
              try {
                jsonData = JSON.parse(data);
              } catch (e) {
                return cb(new VError(err, 'Unable to parse rules file %s', property.tempFile));
              }
              if (err || !jsonData || !jsonData.rules) return cb(new VError(err, 'Error reading rules file %s', property.tempFile));
              return cb(null, _.extend(property, { rules: jsonData.rules }));
            });
          },
          (property, cb) => {
            logger.info('Creating new property version.');
            return createNewPropertyVersion(program, property, cb);
          },
          (property, cb) => {
            logger.info('Updating property rules.');
            updateRules(program, property, cb);
          },
          (property, cb) => {
            logger.info(`Activating property ${propertyName} on staging.`);
            return activatePropertyLatestVersion(program, property, opt, cb);
          },
          (property, cb) => {
            logger.info('Waiting for property to activate on staging.');
            return followPropertyActivation(program, property, cb);
          },
          (property, cb) => {
            logger.info('Property activated on staging network.');
            return cb(null, property);
          },
          (property, cb) => {
            if (!opt.complianceNoProdTraffic) return cb(null, property);
            logger.info('Press any key once ready to proceed with production deployment.');
            return process.stdin.once('data', () => {
              cb(null, property);
            });
          },
          (property, cb) => {
            if (!opt.complianceNoProdTraffic) return cb(null, property);
            logger.info(`Activating property ${propertyName} on production.`);
            return activatePropertyLatestVersion(
              program,
              property,
              _.extend(opt, { production: true }),
              cb,
            );
          },
          (property, cb) => {
            if (!opt.complianceNoProdTraffic) return cb(null, property);
            logger.info('Waiting for property to activate on production.');
            return followPropertyActivation(program, property, cb);
          },
          (property, cb) => {
            if (!opt.complianceNoProdTraffic) return cb(null, property);
            logger.info('Property activated on production network.');
            return cb(null, property);
          },
        ],
        (err, data) => programCb(err, data, program),
      );
    });
};
