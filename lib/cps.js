const _ = require('underscore');
const VError = require('verror');
const logger = require('log4js').getLogger('cps');

const openApi = require('./open-api');

function listEnrollments(program, cb) {
  logger.debug('List enrollments.');
  return openApi(
    program, {
      path: '/cps/v2/enrollments',
      method: 'GET',
      headers: {
        accept: 'application/vnd.akamai.cps.enrollments.v7+json',
      },
    },
    (err, data) => {
      if (err || !data || !data.enrollments || !data.enrollments) {
        return cb(new VError(err, 'Error listing enrollments, no enrollments returned.'));
      }
      return cb(null, data.enrollments);
    },
  );
}

function retrieveEnrollment(program, enrollmentCn, cb) {
  logger.debug('Retrieve enrollment.');
  return listEnrollments(program, (err, enrollments) => {
    if (err) return cb(new VError(err, 'Error retrieving enrollments.'));
    const enrollment = _.find(enrollments, e => e.csr.cn === enrollmentCn);
    if (enrollment === undefined) {
      return cb({
        status: 400,
        message: 'Enrollment not found.',
      });
    }
    return cb(null, enrollment);
  });
}

function retrieveEnrollmentChangeStatus(program, enrollmentCn, cb) {
  logger.debug('Retrieve enrollment change status.');
  return retrieveEnrollment(program, enrollmentCn, (err, enrollment) => {
    if (err) return cb(err);
    if (!enrollment.pendingChanges || enrollment.pendingChanges.length === 0) {
      return cb({
        status: 404,
        message: 'No changes found.',
      });
    }
    return openApi(
      program, {
        path: enrollment.pendingChanges[0],
        method: 'GET',
        headers: {
          accept: 'application/vnd.akamai.cps.change.v2+json',
        },
      },
      (errApi, data) => {
        if (errApi || !data || !data.statusInfo) {
          return cb(new VError(errApi, 'No change status returned.'));
        }
        return cb(errApi, data.statusInfo, program);
      },
    );
  });
}

module.exports.use = function use(program, programCb) {
  logger.debug('Setting up cps command');
  program
    .command('cps-list-enrollments')
    .description('List all enrollments')
    .action(() => {
      logger.info('Listing all enrollments');
      return listEnrollments(program, (err, data) => programCb(err ? new VError(err, 'Error listing enrollments.') : null, data, program));
    });

  program
    .command('cps-get-enrollment <enrollmentCn>')
    .description('Retrieving enrollment')
    .action((enrollmentCn) => {
      logger.info('Retrieving enrollment');
      return retrieveEnrollment(
        program,
        enrollmentCn,
        (err, data) => programCb(err ? new VError(err, 'Error retrieving enrollment.') : null, data, program),
      );
    });

  program
    .command('cps-get-change <enrollmentCn>')
    .description('Retrieving enrollment change status')
    .action((enrollmentCn) => {
      logger.info('Retrieving enrollment change status');
      return retrieveEnrollmentChangeStatus(
        program,
        enrollmentCn,
        (err, data) => programCb(err ? new VError(err, 'Error retrieving enrollment change status.') : null, data, program),
      );
    });
};
