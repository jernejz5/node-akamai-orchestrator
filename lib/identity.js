const https = require('https');
const fs = require('fs');
const _ = require('underscore');
const VError = require('verror');
const logger = require('log4js').getLogger('identity');

const util = require('./util');

function icApi(program, param, cb) {
  const credentials = program.credentials || process.env.IC_DEFAULT.toUpperCase().replace('-', '_');
  logger.debug('Credentials Enviroment variables:', credentials);

  const apiHost = process.env[`IC_${credentials}_API_HOST`];
  logger.debug('Credentials Enviroment variable apiHost:', apiHost);

  const appId = process.env[`IC_${credentials}_APPLICATION_ID`];
  logger.debug('Credentials Enviroment variable appId:', appId);

  const clientId = process.env[`IC_${credentials}_CLIENT_ID`];
  logger.debug('Credentials Enviroment variable clientId:', clientId);

  const clientSecret = process.env[`IC_${credentials}_CLIENT_SECRET`];
  logger.debug('Credentials Enviroment variable clientSecret:', clientSecret);

  if (!apiHost || !appId || !clientId || !clientSecret) {
    return cb(new VError(
      'One of required parameters not defined. appHost: %s, appId: %s, clientId: %s, clientSecret: %s',
      apiHost,
      appId,
      clientId,
      clientSecret,
    ));
  }

  const authorization = `${clientId}:${clientSecret}`;
  logger.trace('Authorization string:', authorization);
  const authorizationBase64 = Buffer.from(authorization).toString('base64');
  logger.trace('Authorization string encoded:', authorizationBase64);

  let headers = {
    'Accept-Content': 'application/json',
    Authorization: `Basic ${authorizationBase64}`,
  };

  if (param.body && param.body.length > 0) {
    headers = _.extend(headers, {
      'Content-Type': 'application/json',
      'Content-Length': param.body.length,
    });
  }

  const opt = {
    hostname: apiHost,
    port: 443,
    path: param.path ? `/config/${appId}${param.path}` : `/config/${appId}`,
    method: param.method || 'GET',
    headers: _.extend(headers, param.headers),
    body: param.body,
  };

  logger.debug('Options for identity API call:', opt);

  return https.get(opt, (res) => {
    res.setEncoding('utf8');
    let rawData = '';

    res.on('data', (chunk) => {
      rawData += chunk;
    });

    res.on('end', () => {
      logger.trace('Raw response data:', rawData);
      let parsedData = null;
      try {
        parsedData = JSON.parse(rawData);
      } catch (e) {
        logger.warn('Unable to parse response body to JSON');
      }

      if (res.statusCode !== 200) {
        return cb(
          new VError(
            'Error response: %s\n%s',
            res.statusCode,
            parsedData ? JSON.stringify(parsedData, null, 2) : rawData,
          ),
        );
      }

      return cb(null, parsedData || rawData);
    });
  });
}

module.exports.use = function use(program, programCb) {
  logger.debug('Setting up identity cloud command');

  program
    .command('identity-api <endpoint>')
    .description('Execute Identity Cloud API Request')
    .option('--method [method]', 'method for api call: GET')
    .option('--header [header]', 'headers to be passed with api call: "Content-Type: application/json"', util.collectOptions, [])
    .option('--query [query]', 'query strings to be passed with api call: none', util.collectOptions, [])
    .option('--data [data]', 'data to be passed with api call: none')
    .option('--file [file]', 'data file containing body to be passed with api call: none')
    .action((endpoint, opt) => {
      if (!endpoint) return new VError('API endpoint is not defined.');
      const headers = _.extend({
        'Content-Type': 'application/json'
      }, util.parseParameterList(opt.header, ':'));
      const queries = util.parseParameterList(opt.query, '=');
      const dataFile = opt.file ? fs.readFileSync(opt.file, 'utf8') : false;

      icApi(
        program, {
          credentials: opt.credentials || 'default',
          path: endpoint,
          method: opt.method || 'GET',
          headers,
          queries,
          body: dataFile || opt.data,
        },
        (err, data) => programCb(
          err ? new VError(err, 'Error during custom API command') : null,
          data,
          program.output ? program : _.extend(program, { output: 'console' }),
        ),
      );
      return null;
    });
};
