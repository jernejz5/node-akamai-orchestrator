const logger = require('log4js').getLogger('idm');
const VError = require('verror');

const openApi = require('./open-api');

module.exports.use = function use(program, programCb) {
  logger.debug('Setting up idm command');
  program
    .command('idm-list-contexts')
    .description('Get GTM domain information')
    .option('--query [query]', 'Search query')
    .option('--identity [identity]', 'Open Identity ID')
    .action((opt) => {
      logger.trace('Retrieving IDM context.');
      return openApi(
        program,
        {
          path: `/identity-management/v1/open-identities/${opt.identity}/account-switch-keys`,
          queries: {
            search: opt.query,
          },
          method: 'GET',
        },
        (err, data) => programCb(new VError('Error retrieving IDM context for identity %s', opt.identity), data, program),
      );
    });
};
