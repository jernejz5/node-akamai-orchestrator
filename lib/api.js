const _ = require('underscore');
const fs = require('fs');
const VError = require('verror');
const logger = require('log4js').getLogger('api');

const openApi = require('./open-api');
const util = require('./util');

module.exports.use = function use(program, programCb) {
  logger.debug('Setting up api command');
  program
    .command('api <endpoint>')
    .description('Execute Open API Request')
    .option('--method [method]', 'method for api call: GET')
    .option('--header [header]', 'headers to be passed with api call: "Content-Type: application/json"', util.collectOptions, [])
    .option('--query [query]', 'query strings to be passed with api call: none', util.collectOptions, [])
    .option('--data [data]', 'data to be passed with api call: none')
    .option('--file [file]', 'data file containing body to be passed with api call: none')
    .action((endpoint, opt) => {
      if (!endpoint) return new VError('API endpoint is not defined.');
      const headers = _.extend({ 'Content-Type': 'application/json' }, util.parseParameterList(opt.header, ':'));
      const queries = util.parseParameterList(opt.query, '=');
      const dataFile = opt.file ? fs.readFileSync(opt.file, 'utf8') : false;

      openApi(
        program,
        {
          credentials: opt.credentials || 'default',
          edgerc: opt.edgerc,
          clientToken: opt.clienttoken,
          clientSecret: opt.clientsecret,
          accessToken: opt.accesstoken,
          host: opt.apihost,
          path: endpoint,
          method: opt.method || 'GET',
          headers,
          queries,
          body: dataFile || opt.data,
        },
        (err, data) => programCb(new VError(err, 'Error during custom API command'), data, program),
      );
      return null;
    });
};
