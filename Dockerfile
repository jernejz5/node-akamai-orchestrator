FROM node

RUN npm install -g https://gitlab.com/jernejz5/node-akamai-orchestrator/

ENTRYPOINT [ "akacli" ]
