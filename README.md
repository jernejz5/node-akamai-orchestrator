
# akacli

This is my personal utility implementation wrapping few Akamai Open APIs and exposing them in functional orchestrated set of commands. Feel free to use, contribute and give suggestions: <https://gitlab.com/jernejz5/node-akamai-orchestrator>

## Installation

```bash
npm install -g node-akamai-orchestrator
```

## Usage guide

Use the following command to get more information on how to use cli:

```bash
~ aka --help
Usage: readme [options] [command]

Command line interface utlity for orchestrating Akamai Open API.

Options:
  --version                                       output the version number
  --output [filename/console/temp/none]           command result should be directed to: console
  --credentials [credentials]                     credential set name in .edgerc file: default
  --context [context]                             Context to be used for the API call. When context is used credential set name is appended with -global: default
  --edgerc [edgerc file path]                     path to .edgerc file: ~/.edgerc
  --clientToken [client token]                    client token to be used instead of credentials from .edgerc
  --clientSecret [client secret]                  client secret to be used instead of credentials from .edgerc
  --accessToken [access token]                    access token to be used instead of credentials from .edgerc
  --host [host]                                   api hostname to be used instead of credentials from .edgerc
  --verbose                                       verbose cli output on standard error output
  --trace                                         trace cli output on standard error output
  -h, --help                                      output usage information

Commands:
  api [options] <endpoint>                        Execute Open API Request
  purge [options]                                 Purge or invalidate content on Akamai Edge servers
  papi-list-contracts                             List all contracts
  papi-list-groups                                List all groups
  papi-list-properties [options]                  List all properties with filters
  papi-get-property [options] <propertyName>      Retrieve property
  papi-compare-versions [options] <propertyName>  Retrieve property
  papi-update-property [options] <propertyName>   Update property version
  papi-activate [options] <propertyName>          Activate a property on Akamai Network
  papi-change [options] <propertyName>            Change property name and deploy it to Akamai Network
  dns-get-zone <zoneName>                         Retrieve a zone configuration
  dns-update-zone [options] <zoneName>            Update a zone configuration
  dns-update-record [options] <zoneName>          Update records
  gtm-get-domain <domain>                         Get GTM domain information
  idm-list-contexts [options]                     Get GTM domain information
  cps-list-enrollments                            List all enrollments
  cps-get-enrollment <enrollmentCn>               Retrieving enrollment
  cps-get-change <enrollmentCn>                   Retrieving enrollment change status

```