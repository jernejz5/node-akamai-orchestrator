const program = require('./bin/aka');
const fs = require('fs');
const log4js = require('log4js');

const logger = log4js.getLogger('readme');

const readme = `
# akacli

This is my personal utility implementation wrapping few Akamai Open APIs and exposing them in functional orchestrated set of commands. Feel free to use, contribute and give suggestions: <https://gitlab.com/jernejz5/node-akamai-orchestrator>

## Installation

\`\`\`bash
npm install -g node-akamai-orchestrator
\`\`\`

## Usage guide

Use the following command to get more information on how to use cli:

\`\`\`bash
~ aka --help
${program.helpInformation()}
\`\`\``;
fs.writeFile('README.md', readme, (err) => {
  if (err) logger.error(err);
});
