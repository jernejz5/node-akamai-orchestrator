#!/usr/bin/env node

const program = require('commander');
const fs = require('fs');
const path = require('path');
const log4js = require('log4js');
const uuid = require('uuid/v1');
const dotenv = require('dotenv');
const VError = require('verror');

const packageInfo = require('../package.json');
const api = require('../lib/api');
const purge = require('../lib/purge');
const papi = require('../lib/papi');
const dns = require('../lib/dns');
const gtm = require('../lib/gtm');
const idm = require('../lib/idm');
const cps = require('../lib/cps');
const identity = require('../lib/identity');

dotenv.config({ path: path.join(__dirname, '..', '.env') });

const logger = log4js.getLogger('cli');

let level = 'INFO';

process.argv.forEach((arg) => {
  if (arg === '--verbose') level = 'DEBUG';
  else if (arg === '--trace') level = 'TRACE';
});

log4js.configure({
  appenders: {
    stdout: { type: 'stdout' },
    stderr: { type: 'stderr' },
  },
  categories: {
    default: { appenders: ['stderr'], level },
  },
});

program
  .version(packageInfo.version, '--version')
  .description('Command line interface utlity for orchestrating Akamai Open API.')
  .option('--output [filename/console/temp/none]', 'command result should be directed to: console')
  .option('--credentials [credentials]', 'credential set name in .env file: default')
  .option('--context [context]', 'Context to be used for the API call. When context is used credential set name is appended with _global: default')
  .option('--verbose', 'verbose cli output on standard error output')
  .option('--trace', 'trace cli output on standard error output')
  .option('--inspect', 'inspect with node debugger');

function programCb(err, result, opt) {
  if (err) {
    logger.error('Command completed with error: ', err.message);
    logger.error(VError.fullStack(err));
    return process.exit(1);
  }

  logger.info('Command completed succesfully.');

  logger.trace('Configured output:', opt.output);

  if (!opt.output || opt.output.toLowerCase() === 'none') {
    return null;
  }

  if (opt.output.toLowerCase() === 'console') {
    logger.info('Result written to console.');
    return console.log(JSON.stringify(result, null, 2)); // eslint-disable-line no-console
  }

  if (opt.output.toLowerCase() === 'temp') {
    const tmpFileName = `/tmp/akaout-${uuid()}.json`;
    return fs.writeFile(tmpFileName, JSON.stringify(result, null, 2), (errWrite) => {
      if (errWrite) return logger.error('Error writing output to file: ', errWrite);
      return logger.info(`Result written to file: ${tmpFileName}`);
    });
  }

  return fs.writeFile(opt.output, JSON.stringify(result, null, 2), (errWrite) => {
    if (errWrite) return logger.error('Error writing output to file: ', errWrite);
    return logger.info(`Result written to file: ${opt.output}`);
  });
}

api.use(program, programCb);
purge.use(program, programCb);
papi.use(program, programCb);
dns.use(program, programCb);
gtm.use(program, programCb);
idm.use(program, programCb);
cps.use(program, programCb);
identity.use(program, programCb);

if (process.argv && process.argv.length === 3 && process.argv[2] === 'test') {
  logger.info('TEST Start');
  program.parse([
    'node',
    'orchestrator',
    'help',
    '--trace',
  ]);
} else {
  program.parse(process.argv);
}


if (!process.argv.slice(2).length) {
  program.outputHelp();
}

module.exports = program;
